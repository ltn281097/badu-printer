import os
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.generics import ListCreateAPIView
# from django.contrib.auth.models import User
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.decorators import api_view
from rest_framework import status


from .models import CustomUsers
# from CoreWeb.s2fa import send_otp
import traceback

import re, random, binascii
import logging

# Create your views here.

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class GetMyAccount(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        print(bcolors.WARNING, get_client_ip(request), bcolors.ENDC)
        data = {}
        status = 500
        validate = "Server is error response."
        try:
            data = list(CustomUsers.objects.filter(pk=request.user.pk).values())[0]
            print(data)
            if data:
                status = 200
                validate = 1
            else:
                status = 401
                validate = "Unauthorized."
        except Exception as e:
            validate = str(e)
        return Response(data={
            "status": status,
            "info": data,
            "validate": validate
        })
