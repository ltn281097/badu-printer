from django.urls import include, path, re_path
from .views import (
    GetMyAccount
)

app_name = 'account'
urlpatterns = [
    path('my-account/', GetMyAccount.as_view(), name="my-account"),
]
