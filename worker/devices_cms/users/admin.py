from django.contrib import admin
from .models import CustomUsers
from django.contrib import admin
from sunmi_devices.models import sunmiDevices

# Register your models here.

admin.site.register(CustomUsers)

admin.site.site_header = 'Badu Lucky Devices Management'                    # default: "Django Administration"
admin.site.index_title = 'Features area'                 # default: "Site administration"
admin.site.site_title = 'Badu Lucky Devices' # default: "Django site admin"
admin.site.site_url = "https://badulucky.com"

@admin.register(sunmiDevices)
class DevicesAdmin(admin.ModelAdmin):
    fields = ('name', 'macID', 'webcam_index', 'notes', 'n_histories_printed', 'queue_orders', 'isAvailable', 'created_at', 'updated_at', 'isEnable')
    list_display = ('name', 'macID', 'webcam_index', 'notes', 'n_histories_printed', 'queue_orders', 'isAvailable', 'created_at', 'updated_at', 'isEnable')
	# list_filter = ('updated_at', 'username')
    search_fields = ('macID', 'histories_printed', 'inProgressing_order', 'name', 'isEnable')
    ordering = ('isEnable', '-created_at',)
    
# admin.site.register(DevicesAdmin)

