from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

class CustomUsers(AbstractUser):
    phone_number = models.CharField(max_length=12)
    badu_id = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    username = models.CharField(unique=True, max_length=255)
    email = models.CharField(unique=True, max_length=255, blank=True, null=True)
    email_verified_at = models.DateTimeField(blank=True, null=True)
    password = models.CharField(max_length=255, blank=True, null=True)
    two_factor_secret = models.TextField(blank=True, null=True)
    two_factor_recovery_codes = models.TextField(blank=True, null=True)
    balance = models.IntegerField(null=True, default=0)
    role = models.IntegerField(null=True, default=0)
    referer = models.BigIntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    phone_verified = models.IntegerField(null=True, default=0)

    class Meta:
        managed = True
        db_table = 'users'