
   
import subprocess
import time

from subprocess import check_output


def home(device):
    adb_command = "adb -s %s shell input keyevent 3" %(device)
    subprocess.call(adb_command)
    time.sleep(1)

def back(device):
    adb_command = "adb -s %s shell input keyevent 4" %(device)
    subprocess.call(adb_command, shell=True)
    time.sleep(1)

def click(device_name, x, y, shell=True):
    # print(f"adb -s {device_name} shell input tap {x} {y}")
    subprocess.call(f"adb -s {device_name} shell input tap {x} {y}", shell=shell)
# def click(device, x, y, sleep = 1):
#     print(device)
#     adb_command = "adb -s %s shell input tap %s %s" %(device, x, y)
#     subprocess.call(adb_command, shell=True)
    # time.sleep(sleep)

def text(device, string, sleep = 1):
    adb_command = "adb -s %s shell input text %s" %(device, string)
    subprocess.call(adb_command, shell=True)
    # time.sleep(sleep)

def input_number_from_keyEvent(device, number:int, sleep=1):
    adb_command = f"adb -s %s shell input keyevent {7+number}" %(device)
    subprocess.call(adb_command, shell=True)
    time.sleep(sleep)

def enter(device, sleep = 1):
    adb_command = "adb -s %s shell input keyevent 66" %(device)
    subprocess.call(adb_command, shell=True)
    time.sleep(sleep)

def scroll_home(device):
    adb_command = "adb -s %s shell input keyevent KEYCODE_MOVE_HOME" %(device)
    subprocess.call(adb_command)

def scroll_end(device):
    adb_command = "adb -s %s shell input keyevent 123" %(device)
    subprocess.call(adb_command)

def page_up(device, sleep=0):
    adb_command = "adb -s %s shell input keyevent KEYCODE_PAGE_UP" %(device)
    subprocess.call(adb_command)
    time.sleep(sleep)

def page_down(device, sleep=0):
    adb_command = "adb -s %s shell input keyevent KEYCODE_PAGE_DOWN" %(device)
    subprocess.call(adb_command)
    time.sleep(sleep)

def install_apk(device, apk_path):
    adb_command = "adb -s %s install %s" %(device, apk_path)
    subprocess.call(adb_command)

def set_clipboard(device, text):
    # adb_command = "adb shell am broadcast -s %s clipper.set -e text %s" %(device, text)
    adb_command = "adb shell am broadcast -a clipper.set -e text '%s'" %(text)  
    subprocess.call(adb_command)

def paste(device):
    adb_command = "adb -s %s shell input keyevent KEYCODE_PASTE" %(device)
    subprocess.call(adb_command)

def copy(device):
    adb_command = "adb -s %s shell input keyevent KEYCODE_COPY" %(device)
    subprocess.call(adb_command)

def clear_cache_mozilla(device):
    adb_command = "adb -s %s shell pm clear org.mozilla.fenix" %(device)
    subprocess.call(adb_command)
    adb_command = "adb -s %s shell pm clear org.mozilla.firefox" %(device)
    subprocess.call(adb_command)

def power_button(device):
    adb_command = "adb -s %s shell input keyevent KEYCODE_POWER" %(device)
    subprocess.call(adb_command)

def reset_network(device):
    adb_command = "adb -s %s shell svc data disable" %(device)
    subprocess.call(adb_command)
    time.sleep(2)
    adb_command = "adb -s %s shell svc data enable" %(device)
    subprocess.call(adb_command)

def get_clipboard(device):
    text = check_output(["adb", "-s", device, "shell", "am broadcast -a clipper.get"])
    return text.decode("utf-8").split("\"")[1]

def set_clipboard(device):
    adb_command = "adb -s %s shell am broadcast -a clipper.set -e text 'https://accounts.google.com/speedbump/'" %(device)
    subprocess.call(adb_command)

def screenshoot(device, img_name):
    adb_command1 = f"adb -s {device} shell screencap -p /mnt/sdcard/Pictures/{img_name}.png"
    subprocess.call(adb_command1)
    time.sleep(2)
    adb_command2 = f"adb -s {device} pull /mnt/sdcard/Pictures/{img_name}.png order/screenshoot/{img_name}.png"
    subprocess.call(adb_command2)
    return "order/screenshoot/{img_name}.png"