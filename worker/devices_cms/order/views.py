import traceback
import os
from decorator_corlors import bcolors

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from time import sleep

from .models import OrdersQueue, ActionPrinter
from sunmi_devices.models import sunmiDevices

class PrinterAction(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request, macId):
        """
            /api/v1/printer/<str:macID>
            method: Post
            body: "" not required.
            headers: Authorization
            Bearer Token...

            expected : 
            "
                {
                    "name":
                    "id":
                    "data": [
                        {
                            "numbers":
                            "prices":
                        },
                    ]
                }
            "
        """
        responser = {}
        try:
            webcam_index = sunmiDevices.objects.filter(macId=macId)[0].webcam_index
            data = {"name": macId, "webcam_index": webcam_index}
            data.update(request.json)
            PrinterAction.update_for_list_offset(macId, data)
            status=200
        except Exception:
            traceback.print_exc()
            status=400
        return Response(responser, status=status)

    @staticmethod
    def update_for_list_offset(macId, data):
        orders_list = OrdersQueue.objects.all()[0].orders_list
        orders_list = eval(orders_list)
        if macId in orders_list:
            orders_list[macId].append(data)
        else:
            orders_list[macId]=[data]
        return OrdersQueue.objects.all().update(orders_list=str(orders_list))

    @staticmethod
    def put(request, macId):
        ActionPrinter.objects.filter(macId=macId).update(status=1)
        # sleep(1)
        # threading.Thread(target=PrinterAction.handle_actioner, args=(macId, ), daemon=True).start()
        return Response({"msg": f"Running {macId} - Success"}, status=200)

    @staticmethod
    def delete(request, macId):
        ActionPrinter.objects.filter(macId=macId).update(status=0)
        return Response({"msg": f"Stop {macId} - Success"}, status=200)