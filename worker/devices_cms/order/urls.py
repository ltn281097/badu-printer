from django.urls import include, path, re_path
from .views import (
    PrinterAction
)

app_name = 'order'
urlpatterns = [
    path('printer/<str:macID>', PrinterAction.as_view(), name="action-printer"),
]
