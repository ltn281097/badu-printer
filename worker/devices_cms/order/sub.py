
from ast import Index
from queue import Queue
from paho.mqtt import client as mqtt_client
from .config import *
from memory_profiler import profile
from requests import post, get, put
from threading import Thread
from time import sleep
import hashlib, os, traceback

from sunmi_devices.models import sunmiDevices
from .models import ActionPrinter, OrdersQueue
from decorator_corlors import bcolors
from .worker import actioner


def connect_mqtt(client_id, alive=1) -> mqtt_client:
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            client.connected_flag = True
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)
    client = mqtt_client.Client(client_id, clean_session=False)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    # client.connect_async()
    if alive:
        client.connect(broker, port, keepalive=10)
    else:
        client.connect(broker, port, )
    return client


def encode_sha256(string):
    encoded=string.encode()
    result = hashlib.sha256(encoded)
    return result.hexdigest()

def action_from_msg_printer(msg_txt):
    body_msg = eval(msg_txt)
    try:
        devices = sunmiDevices.objects.filter(isEnable=1).order_by('queue_orders')
        print("devices ordered --- ", devices)
        device_mac_ID = 0
        for device in devices:
            device_mac_ID = device.macID
            break
        if device_mac_ID:
            post_action(device_mac_ID, body_msg)
            sleep(1)
            handle_actioner(device_mac_ID)
        else:
            pass
    except Exception as e:
        print(e)

def post_action(macId, body):
    """
        GET infos sunmi device -> update data
    """
    print("GET infos sunmi device -> update data", macId, body)
    try:
        webcam_index = sunmiDevices.objects.filter(macID=macId)[0].webcam_index
        data = {"name": macId, "webcam_index": webcam_index}
        data.update(body)
        update_for_list_offset(macId, data)
    except Exception:
        traceback.print_exc()

def update_for_list_offset(macId, data):
    try: 
        OrdersQueue.objects.all()[0]
    except IndexError:
        orders_list = {}
        orders_list[macId]=[data]
        return OrdersQueue.objects.create(orders_list=orders_list)
    orders_list = OrdersQueue.objects.all()[0].orders_list
    orders_list = eval(orders_list)
    if macId in orders_list:
        orders_list[macId].append(data)
    else:
        orders_list[macId]=[data]
    x = sunmiDevices.objects.filter(macID=macId)[0]
    x.queue_orders += 1
    x.save()
    return OrdersQueue.objects.all().update(orders_list=str(orders_list))

def dequeue(macId):
    Queue_list = list(OrdersQueue.objects.all().values())[0].get("orders_list")
    print("Queue_list >> ", Queue_list)
    try:
        if not isinstance(Queue_list, dict):
            Queue_offset = eval(Queue_list)
        else:
            Queue_offset = Queue_list.copy()
        Queue_list = Queue_offset.get(macId)
        print(Queue_list)
        if not Queue_list:
            return
        data = Queue_list.pop(0)
        print(data)
        OrdersQueue.objects.all().update(orders_list=str(Queue_offset))
    except TypeError:
        print(Queue_list, type(Queue_list))
        data = {}
    return data

def handle_actioner(macId):
    n = 0
    while 1:
        sleep(2)
        try:
            if not sunmiDevices.objects.filter(macID=macId)[0].isAvailable:
                "Running exists!"
                return                
        except:
            traceback.print_exc()
            return
        try:
            data = dequeue(macId)
            
            print("data:  >>> ", data)
            if not data:
                continue
            if data:
                sunmiDevices.objects.filter(macID=macId).update(isAvailable=0)
            folder_id = f'media/{data["id_order"]}'
            try: os.mkdir(folder_id)
            except Exception: pass
            print(data)
            Thread(target=print_threading(macId, data), daemon=True).start()
            n+=1
            print(bcolors.OKBLUE, f"Success: {n}: ", data, bcolors.ENDC)
        except KeyError:
            print(bcolors.FAIL, "id_order is required.", bcolors.ENDC)
        except Exception:
            traceback.print_exc()
            print(bcolors.FAIL, "ERROR: ", data, bcolors.ENDC)

def print_threading(macId, data):
    actioner(**data)
    Queue_list = list(OrdersQueue.objects.all().values())[0].get("orders_list")
    Queue_offset = eval(Queue_list)
    Queue_list = Queue_offset.get(macId)
    if not Queue_list:
        sunmiDevices.objects.filter(macID=macId).update(isAvailable=1)

@profile
def subscribe(client: mqtt_client, topic):
    def on_message(client, userdata, msg):
        handle_threading_main(client, msg)

    def handle_threading_main(client, msg):
        msg_txt = msg.payload.decode()
        try:
            if isinstance(eval(msg_txt), dict):
                Thread(target=action_from_msg_printer, args=(msg_txt,), daemon=True).start()
        except Exception:
            try:
                Queue_list = list(OrdersQueue.objects.all().values())[0].get("orders_list")
                Queue_offset = eval(Queue_list)
                return client.publish(msg_txt, str(Queue_offset))
            except:
                traceback.print_exc()
        
    client.subscribe(topic)
    client.on_message = on_message
    client.loop_forever()


def run(client_id, topic):
    try:
        client = connect_mqtt(client_id)
    except ConnectionRefusedError:
        print("Password invalid!")
        return
    msg = subscribe(client, topic)
    # while 1:
    #     pass
    return msg

if __name__ == '__main__':
    client_id = "client_printer_01"
    topic="client_printer_01"
    run(client_id, topic)