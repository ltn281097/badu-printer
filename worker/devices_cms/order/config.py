import json
with open("order/config.json", "r") as f:
    data = json.load(f)
    print(data)
mod = data.get("mod")
broker = mod.get("host")
port = mod.get("port")
username = mod.get("username")
password = mod.get("password")