from django.db import models

# Create your models here.
class OrdersQueue(models.Model):
    orders_list = models.TextField(default='{}', help_text="offset of list ofset")

class ActionPrinter(models.Model):
    macId = models.CharField(max_length=255)
    status = models.BooleanField(default=0)