import traceback
from ppadb.client import Client as AdbClient
from .lib import *
from .lib_coordinates import *
from time import time, sleep
from pprint import pprint
from requests import patch, post, put
import threading, os, hashlib

from .stored import put_to_bucket_file
from decorator_corlors import bcolors
from .stored import env_alibaba
from sunmi_devices.models import sunmiDevices
import cv2

DELAY_DEFAULT = float(env_alibaba('DELAY_DEFAULT'))

class PostOrder:
    def __init__(self, name, instance, host="127.0.0.1", port=5037):
        self.client = AdbClient(host, port)
        self.name = name
        self.device = self.client.device(name)
        self.instance = instance
        print(f'Connected to {self.device}')

    def go_to_inside_app(self, delay_step=DELAY_DEFAULT):
        click(self.name, *self.instance.button_open_lao_lotto, delay_step)
        click(self.name, *self.instance.input_password, delay_step)
        text(self.name, self.instance.password, delay_step)
        click(self.name, *self.instance.button_login, delay_step)

    def post_order_digit_type(self, number_string, pass_taken=0, delay_step=DELAY_DEFAULT):
        number_string = str(number_string)
        if not pass_taken:
            click(self.name, *self.instance.button_digital_type, delay_step)
            click(self.name, *self.instance.button_cancel_ok, delay_step)
        for i in number_string:
            threading.Thread(target=click, args=(self.name, *eval(f"self.instance.button_number_{i}"))).start()
            sleep(delay_step)
    
    def post_order_amount_enter(self, money, delay_step=DELAY_DEFAULT):
        money = str(money)
        click(self.name, *self.instance.input_amount_money, delay_step)
        if '000' in money:
            money = money.replace("000", "a")

        for i in money:
            if i == 'a':
                i = '000'
            threading.Thread(target=click, args=(self.name, *eval(f"self.instance.button_number_{i}"))).start()
            sleep(delay_step)
            

    def post_order_sale(self, delay_step=DELAY_DEFAULT):
        click(self.name, *self.instance.button_sale, delay_step)

    def post_order_print(self, id, delay_step=DELAY_DEFAULT):
        click(self.name, *self.instance.button_printer, delay_step)
        sleep(1)
        click(self.name, *self.instance.button_done_ok, delay_step)

def action_post_order(X, numbers:list, prices:list, id:str, webcam_index=0):
    a = time()
    for index_n, element in enumerate(list(zip(numbers, prices))):
        number, amount = element
        number = str(number)
        amount = str(amount)
        if index_n==0:
            X.post_order_digit_type(number, pass_taken=0)
            if len(number)!=6:
                sleep(DELAY_DEFAULT)
            else:
                sleep(DELAY_DEFAULT)
            X.post_order_amount_enter(amount)
            sleep(DELAY_DEFAULT)
            X.post_order_sale()
        else:
            X.post_order_digit_type(number, pass_taken=1)
            if len(number)!=6:
                sleep(DELAY_DEFAULT)
            else:
                sleep(DELAY_DEFAULT)
            X.post_order_amount_enter(amount)
            sleep(DELAY_DEFAULT)
            X.post_order_sale()
    X.post_order_print(id=id)
    sleep(1)
    """img_future_status = screenshoot(X.name, id)
    bug_shoot = cv2.imread("order/bugShoot/draw_is_closed.jpg")
    img_future_status = cv2.imread(img_future_status)
    result_compare_bug =  cv2.matchTemplate(img_future_status, bug_shoot, cv2.TM_SQDIFF_NORMED)
    print(": BUG : Comparing draw_is_closed -- ", result_compare_bug)"""
    # if result_compare_bug:
    #     print("DRAW IS CLOSED")
    #     return 0
    sleep(1)
    
    print(f"mosquitto_pub -h m15.cloudmqtt.com -P WmW1cGTSpPo0 -u qbdtmkly -m acapture/forward{id}-{webcam_index} -t badu/test/{1 if webcam_index==0 else 2} -p 17286")
    subprocess.call(f"mosquitto_pub -h m15.cloudmqtt.com -P WmW1cGTSpPo0 -u qbdtmkly -m acapture/forward{id} -t badu/test/{1 if webcam_index==0 else 2} -p 17286", shell=True)
    b = time()
    pprint(list(zip(numbers, prices)))
    print("Time - computational: ", b-a)

def microservice_validate(numbers, urls, id_order, headers):
    try:
        validation = post("http://127.0.0.1:7878/api/v1/service/validate/multiple", verify=False, json={
            "numbers": numbers,
            "url_imgs": urls
        }).json().get("validation")
        print(bcolors.WARNING, validation, bcolors.ENDC)
        if validation:
            validate = "VALIDATED"
        else:
            validate = "INVALIDATE"  
        put(
            f"{env_alibaba('HOST_BADU_LUCKY')}/payment/external/update-ticket-capture/{id_order}", 
            verify= False,
            json={
                "captureStatus": validate,
            },
            headers=headers,
        )
        post(
            "http://127.0.0.1:7979/api/v1/printer/test-post-imgs", 
            verify= False,
            json={
                "captureStatus": validate,
            },
            headers=headers,
        )
        
    except Exception:
        print("micro service AI- detection: ", Exception)

def str2h256(string):
    h = hashlib.sha256(str(string).encode('utf-8'))
    return h.hexdigest()

def actioner(id_order, data, name = "VS44219G51997", total=0, device_id=0, webcam_index=0):
    device_id = int(device_id)
    folder_id = f"order/media/{id_order}"
    try: os.mkdir(folder_id)
    except: pass
    instance = AppConfiguration()
    X = PostOrder(name, instance)
    numbers = []
    for offset in data:
        action_post_order(X, **offset, id=id_order, webcam_index=webcam_index)
        numbers+=offset["numbers"]
        sleep(10)
    begin_time = time()
    while 1:
        if time()-begin_time>20*total:
            break
        urls = []
        path_imgs = os.listdir(folder_id)
        if len(path_imgs)==total:
            sleep(1)
            for path_img in path_imgs:
                path_img = os.path.join(folder_id, path_img)
                url = put_to_bucket_file(f"{id_order}/{str2h256(path_img)}", path_img)
                urls.append(url)
            break

    token = f"{id_order}-Badu-Lucky-2022"
    headers={
        "token": str2h256(token)
    }
    try:
        threading.Thread(
            target=microservice_validate, 
            args=(numbers, urls, id_order, headers), 
            daemon=True).start()
    except Exception as e:
        print(bcolors.FAIL, e, bcolors.ENDC)
    # try:post(f"http://192.168.1.3:4030/payment/external/receive-ticket-capture/{id_order}", json={"image_urls": urls}, headers=headers, verify=False, timeout=2)
    # except Exception: pass
    try:post(f"{env_alibaba('HOST_BADU_LUCKY')}/payment/external/receive-ticket-capture/{id_order}", json={"image_urls": urls}, headers=headers, verify=False, timeout=2)
    except Exception: pass
    # try:post(f"http://127.0.0.1:7979/api/v1/printer/test-post-imgs", json={"image_urls": urls}, headers=headers, timeout=2) #timeout=1)
    # except: pass
    X = sunmiDevices.objects.filter(macID=name)[0]
    X.isAvailable=1
    X.queue_orders-=1
    X.n_histories_printed+=1
    X.save()


if __name__ == '__main__':
    name = "VS44219G51997"
    instance = AppConfiguration()
    X = PostOrder(name, instance)
    from random import randint, choice
    for i in range(1,2):
        numbers = [str(j) for j in range(10*i, 10*(i+1))]
        prices = [choice(["2000", "2000", "2000", "2000"]) for _ in range(10)]
        action_post_order(X, numbers, prices, id=id(numbers))

