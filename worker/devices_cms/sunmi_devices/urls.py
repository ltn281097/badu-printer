from django.urls import include, path, re_path
from .views import (
    devicesSunmiViews
)

app_name = 'account'
urlpatterns = [
    path('devices/sunmi', devicesSunmiViews.as_view(), name="devices-get"),
]
