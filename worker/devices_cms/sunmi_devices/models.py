from distutils.text_file import TextFile
from django.db import models
from django.utils.timezone import now
from django_mysql.models import ListCharField


# Create your models here.
class sunmiDevices(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=20, blank=True, null=True)
    macID = models.CharField(max_length=20, blank=True, null=False, unique=True)
    notes = models.CharField(max_length=100, blank=True, null=True)
    histories_printed = models.TextField(blank=True, null=True, default='')
    n_histories_printed = models.IntegerField(default=0)
    inProgressing_order = models.JSONField(blank=True, null=True, default=dict)
    queue_orders = models.IntegerField(blank=True, null=True, default=0)
    isAvailable = models.BooleanField(default=0, blank=True, null=True)
    webcam_index = models.IntegerField(unique=True, null=False)
    created_at = models.DateTimeField(default=now)
    updated_at = models.DateTimeField(default=now)
    isEnable = models.BooleanField(default=1)

    class Meta:
        managed = True
        db_table = 'sunmi_devices'
        ordering = ['isEnable', '-created_at']

    def __str__(self,):
        return self.macID



