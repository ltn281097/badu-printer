from django.apps import AppConfig


class SunmiDevicesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sunmi_devices'
