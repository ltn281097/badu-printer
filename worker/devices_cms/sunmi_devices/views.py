from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

import traceback

from order.sub import run as subcribe_run
from .models import sunmiDevices

from django.utils.timezone import now
from threading import Thread

import json

class devicesSunmiViews(APIView):
    
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request):
        """
            /api/v1/devices/sunmi
            method: GET
            params: "is_all" not required.
            headers: Authorization
            Bearer Token...
        """
        responser = {}
        try:
            if request.user.is_superuser:
                is_all = request.GET.get("is_all")
                if is_all:
                    devices = sunmiDevices.objects.filter(isEnable=1).order_by('-isAvailable').values()
                else:
                    devices = sunmiDevices.objects.filter(isEnable=1).order_by('-isAvailable').values()[0]
                responser["devices"] = devices
                return Response(responser, status=200)
            else:
                responser["msg"] = "NOT FORBIDEN."
        except IndexError:
            responser["msg"]=["Not devices Available."]
            status=200
        except Exception as e:
            responser["validate"]=str(e)
            status=400
        return Response(responser, status=status)
    
    @staticmethod
    def post(request):
        responser={}
        try:
            if request.user.is_superuser:
                body = request.body
                if isinstance(body, str):
                    body = eval(body)
                elif isinstance(body, bytes):
                    body = eval(body.decode())
                print(body, type(body))
                client_id = body["client_id"]#"client_printer_01"
                topic=body["topic"]#"client_printer_01"
                Thread(target=subcribe_run(client_id, topic), daemon=True).start()
                status=200
                responser["msg"] = "Running Success..."
            else:
                responser["msg"] = "Not permission."
                status=403
        except KeyError:
            status=400
            responser["validate"] = "invalid requests."
        except Exception:
            traceback.print_exc()
            status=500
        return Response(responser, status=status)

    @staticmethod
    def put(request):
        """
            /api/v1/devices/sunmi
            method: PUT
            params: "macID", "id", "isAvailable" is required.
            headers: Authorization
            Bearer Token...
        """
        responser = {}
        try:   
            macID = request.data["macID"]
            id = request.data["id"]
            isAvailable = request.data["isAvailable"]
            if request.user.is_superuser and macID:
                sunmiDevices.objects.filter(id=id, macID=macID).update(isAvailable=isAvailable)
                responser["status"] = True
                responser["msg"] = "Success"
                status = 202
            else:
                responser["msg"] = "NOT FORBIDEN."
                status=403
        except KeyError:
            responser["msg"] = "Invalid request."
            responser["status"] = False
            status = 400
        except Exception as e:
            traceback.print_exc()
            status=400
        return Response(responser, status=status)