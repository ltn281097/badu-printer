from django.contrib.auth import authenticate, login
from django.contrib.auth.password_validation import password_changed
from django.http.response import JsonResponse
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm

from users.models import CustomUsers
from django.views.decorators.csrf import csrf_exempt
from rest_framework_simplejwt.tokens import RefreshToken

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

# from .s2fa import send_otp

def get_tokens_for_user(user):
    refresh = RefreshToken.for_user(user)

    return {
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }

@csrf_exempt
def auth_view(request):
    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")
        user = authenticate(request, username=username, password=password)
        if user is not None:
            request.session['pk'] = user.pk
            pk = request.session.get("pk")
            user = CustomUsers.objects.get(pk=pk)
            login(request, user)
            return JsonResponse(get_tokens_for_user(user))
        else:
            detail = "UnAuthented"     
    else:
        detail = "UnAuthented" 
    return JsonResponse({
        "detail": detail,
        "link_url": "/api/v1/login/verify/"
    })