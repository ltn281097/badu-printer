import environ
import oss2
import traceback

# Initialise environment variables
env_alibaba = environ.Env()
environ.Env.read_env("./.env")

#######
def init_bucket():
    auth = oss2.Auth(env_alibaba('ALIBABA_ID'), env_alibaba('ALIBABA_SECRET_KEY'))
    bucket = oss2.Bucket(auth, env_alibaba('ALIBABA_ENDPOINT'), env_alibaba('ALIBABA_BUCKET'))
    return bucket

def put_to_bucket(img_id, base64string, url=""):
    try:
        bucket = init_bucket()
        result = bucket.put_object(f'{img_id}.txt', base64string)
        result_img = bucket.get_object_to_file(f'{img_id}.jpg', url)
        print(result, result_img)
        return result
    except Exception as e:
        traceback.print_exc()
        print(e)

def put_to_bucket_file(img_id, filename, url=""):
    try:
        bucket = init_bucket()
        bucket.put_object_from_file(f'{img_id}.jpg', filename)
        return url
    except Exception as e:
        traceback.print_exc()
        print(e)
        
def get_store_backup(img_id, save_file=0, filename="", url=""):
    try:
        bucket = init_bucket()
        if save_file and filename:
            bucket.get_object_to_file(f'{img_id}.txt', filename)
        elif save_file and not filename:
            bucket.get_object_to_file(f'{img_id}.txt', f'content/{img_id}.txt')
        elif not save_file:
            result = bucket.get_object(f'{img_id}.txt').read()
            bucket.get_object_to_file(f'{img_id}.jpg', url)
            if isinstance(result, bytes):
                result = result.decode('utf-8')
        # return { "data": result, "img_id": img_id}
        return {"url": url, "img_id": img_id}
    except Exception as e:
        print(e)
        return {}

if __name__ == '__main__':
    # put_to_bucket(img_id="1", base64string="ascsacsacscsac")
    print(get_store_backup("1"))
    