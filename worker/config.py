import json
with open("./config.json", "r") as f:
    data = json.load(f)
mod = data.get("mod")
broker = mod.get("host")
port = mod.get("port")
username = mod.get("username")
password = mod.get("password")