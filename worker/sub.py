
import traceback
from paho.mqtt import client as mqtt_client
from config import *
from worker import capture2string
from memory_profiler import profile
from threading import Thread

def connect_mqtt(client_id, alive=1) -> mqtt_client:
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            client.connected_flag = True
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)
    client = mqtt_client.Client(client_id, clean_session=False)
    print(username, password)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    if alive:
        client.connect(broker, port, keepalive=10)
    else:
        client.connect(broker, port, )
    return client

import hashlib
def encode_sha256(string):
    encoded=string.encode()
    result = hashlib.sha256(encoded)
    return result.hexdigest()


@profile
def subscribe(client: mqtt_client, topic):
    def on_message(client, userdata, msg):
        try: return Thread(target=handle_msg, args=(msg,), daemon=True).start()
        except: traceback.print_exc()

    def handle_msg(msg):
        msg_txt = msg.payload.decode() 
        msg_txt = msg_txt.split("/")
        id_msg = msg_txt[0]
        webcam_index = int(msg_txt[-1])
        return capture2string(id_msg, webcam_index)
    client.subscribe(topic)
    client.on_message = on_message
    client.loop_forever()


def run(client_id, topic):
    try:
        client = connect_mqtt(client_id)
    except ConnectionRefusedError:
        print("Password invalid!")
        return
    msg = subscribe(client, topic)
    while 1:
        pass
    return msg

if __name__ == '__main__':
    client_id = "test1"
    topic="client1"
    run(client_id, topic)