import cv2
import os
from time import time, sleep
from numpy import any

def capture2string(id_image, position_cam=0, width=640, heigh=480):
    position_cam = 1 if position_cam == 0 else 0
    cam = cv2.VideoCapture(position_cam)
    
    cam.set(cv2.CAP_PROP_FRAME_WIDTH, width)
    cam.set(cv2.CAP_PROP_FRAME_HEIGHT, heigh)
    sleep(10)
    try: os.mkdir(f"devices_cms/order/media/{id_image}")
    except: pass
    for _ in range(20):
        _, frame = cam.read()
        id_time = time()
        filename = f"devices_cms/order/media/{id_image}/{id(id_time)}{str(id_time).replace('.','a')}.jpg"
        print(filename)
        if any(frame):
            frame = cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE)
            cv2.imwrite(filename, frame)
        else:
            continue
        break
    cam.release()
    cv2.destroyAllWindows()
    return 1