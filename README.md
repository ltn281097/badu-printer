**Tài liệu hướng dẫn sử dụng CMS - Phần mềm quản lý Printer

1. CMS
   1. Login
      1. Account:

         1. Username:
         2. Password:
      2. Domain:
      3. [http://127.0.0.1:8000/admin](http://127.0.0.1:8000/admin)
      4. UI:![](https://lh3.googleusercontent.com/OZLpXnLgw6HdbpDT2cfNs6-T1O8rWxS6dTV2MYXeo7i19GXD6w9fBuBNnQwJ6IkINT0mzN_f3_xLIikBxmcH6rrLCAUCdmmhHfNWyYF13PngOlyye5-kLRdal6lHcyUHbrTjBhPw5nTttBEX-w)

![](https://lh5.googleusercontent.com/oe3otgQvz6OEt172Hzkd3jsKphnDrcyoXEaFkueAYJ2Ho9HhGhNFv9CPrrcziw7q3JmgkykEOs38JjFvUL1vN7P5MqeswGSqXWpA1xtQMdg6ErIGC8Bw6NXMvDb6OJJ8cBrmvGR1tk_BpfAtRA)2. PRINTER UI Management:

![](https://lh3.googleusercontent.com/cltLxHqV2GfiOqkxHjFkmWU827AE-56eM0SvTUBtBiAOj9ksZr-xQJV7QwtBczaFXZOPNBkjU8ypwDb-CHJ5oGxHMj_gOfLdNXpvEmF_tXegbSbBQrToOsMxnA-1TlOYCxdllHLhbMzXgThq2w)

3. Vận hành:
   1. Bắt đầu phiên Draw mới:

      1. Bước 1: Chọn 4/40
      2. Animal -> Chọn số tiền
      3. Click random
      4. Kéo giấy đến thiết bị cuốn cuối
   2. Hết giấy:
   3. Vào cms
   4. Set isEnabled -> 0 cho thiết bị cần thay giấy
   5. Lỗi invalid Digital:
   6. Kiểm tra số tiền,
   7. Chụp lại màn hình ngay lúc lỗi để gửi kỹ thuật
   8. Báo bộ phận kỹ thuật
   9. Mất nguồn điện
   10. Báo bộ phận kỹ thuật
   11. Máy dừng in
   12. Báo bộ phận kỹ thuật

**
