#include <TimedAction.h>
#include <Stepper.h>

const int stepsPerRevolution = 2048;
String data;
char *name=NULL;
String condition_forward = "OK run/forward";
String condition_reverse = "OK run/reverse";

void right_dc_891011_forward(int motorSpeed);
void right_dc_891011_reverse(int motorSpeed);
void left_dc_4567_forward(int motorSpeed);
void left_dc_4567_reverse(int motorSpeed);


Stepper rightStepper = Stepper(stepsPerRevolution, 8, 10, 9, 11);
Stepper leftStepper = Stepper(stepsPerRevolution, 4, 6, 5, 7);


void setup() {
  Serial.begin(115200);
}

void right_dc_891011_forward(int motorSpeed){
  rightStepper.setSpeed(15);
    Serial.println("wise - right");
    rightStepper.step(stepsPerRevolution);
    rightStepper.step(stepsPerRevolution/2);
}

void left_dc_4567_forward(int motorSpeed){
  
  leftStepper.setSpeed(motorSpeed);
    float a = millis();
    Serial.println("wise - left");
    leftStepper.step(stepsPerRevolution);
    leftStepper.step(stepsPerRevolution/2);
    float stepTime = (0.0125/motorSpeed)*1000*3*stepsPerRevolution/2;
    Serial.println(millis()-a);
    delay(stepTime*2+2);
    Serial.println("OK DONE");
    Serial.println("OK DONE");
}

void left_dc_4567_reverse(int motorSpeed){
  leftStepper.setSpeed(motorSpeed);
    float a = millis();
    Serial.println("countwise - left");
    leftStepper.step(-stepsPerRevolution);
    leftStepper.step(-stepsPerRevolution/2);
    float stepTime = (0.0125/motorSpeed)*1000*3*stepsPerRevolution/2;
    Serial.println(millis()-a);
    Serial.println("OK DONE");
    Serial.println("OK DONE");
}

void right_dc_891011_reverse(int motorSpeed){
  rightStepper.setSpeed(motorSpeed);
    float a = millis();
    Serial.println("countwise - right");
    rightStepper.step(-stepsPerRevolution);
    rightStepper.step(-stepsPerRevolution/2);
    float stepTime = (0.0125/motorSpeed)*1000*3*stepsPerRevolution/2;
    Serial.println(millis()-a);
    Serial.println("OK DONE");
    Serial.println("OK DONE");
}

void loop() {
  if (Serial.available()>0)
    {
      data = Serial.readString(); //Read the Serial data and store it
      int index_forward = data.indexOf(condition_forward);
      int index_reverse = data.indexOf(condition_reverse);
      if (index_forward>0){
        String sub_S = data.substring(index_forward+condition_forward.length()+1,data.length());
        int speedway = sub_S.toInt();
        Serial.println(sub_S);
        left_dc_4567_forward(15);

      }
      else if (index_reverse>0){
        String sub_S = data.substring(index_reverse+condition_reverse.length()+1,data.length());
        int speedway = sub_S.toInt();
        Serial.println(sub_S);
        left_dc_4567_reverse(15);
        right_dc_891011_reverse(15);
      }
    }
}
