#include "EspMQTTClient.h"
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

const char* serverName = "http://159.223.81.151:8089/capture?id_img=";

String condition_forward = "capture/forward";
String id_string = "1";

EspMQTTClient client(
  "UNITEL_BA DU GROUP",
  "66666666",
  "m15.cloudmqtt.com",  // MQTT Broker server ip
  "qbdtmkly",   // Can be omitted if not needed
  "WmW1cGTSpPo0",   // Can be omitted if not needed
  "TestClient1",     // Client name that uniquely identify your device
  17286              // The MQTT port, default to 1883. this line can be omitted
);

String httpGETRequest(String serverName, String id) {
  WiFiClient client;
  HTTPClient http;
  
  // Your IP address with path or Domain name with URL path 
  http.begin(client, serverName+id);
  http.setTimeout(10000);
  // Send HTTP POST request
  int httpResponseCode = http.GET();
  
  String payload = "{}"; 
  
  if (httpResponseCode>0) {
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
    payload = http.getString();
  }
  else {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }
  // Free resources
  http.end();

  return payload;
}

void setup()
{
  Serial.begin(115200);
  // Optional functionalities of EspMQTTClient
  client.enableDebuggingMessages(); // Enable debugging messages sent to Serial output
  client.enableHTTPWebUpdater(); // Enable the web updater. User and password default to values of MQTTUsername and MQTTPassword. These can be overridded with enableHTTPWebUpdater("user", "password").
  client.enableOTA(); // Enable OTA (Over The Air) updates. Password defaults to MQTTPassword. Port is the default OTA port. Can be overridden with enableOTA("password", port).
  client.enableLastWillMessage("TestClient/lastwill", "I am going offline");  // You can activate the retain flag by setting the third parameter to true
  client.setKeepAlive(10);
}

// This function is called once everything is connected (Wifi and MQTT)
// WARNING : YOU MUST IMPLEMENT IT IF YOU USE EspMQTTClient
void onConnectionEstablished()
{
  // Subscribe to "badu/test" and display received message to Serial
  client.subscribe("badu/test", [](const String & payload) {
    int index_forward = payload.indexOf(condition_forward);
    if (index_forward>0){
      id_string = payload.substring(index_forward+condition_forward.length(),payload.length());
      Serial.print("OK run/forward,20");
      Serial.print(id_string);
    }
    else if(payload.indexOf("capture/reverse")>0){
      Serial.print("OK run/reverse,20");
    }
    unsigned long begin_time = millis();
    while(1){
      if (Serial.available()>0){
        String msg = Serial.readString();
        if (msg.indexOf("OK DONE")>0){
          String responser = httpGETRequest(serverName, id_string);
          Serial.println("OKEEEE CAPTURE");
          Serial.println((millis()-begin_time));
          Serial.println(responser);
          break;
        }
        else {
          Serial.println((millis()-begin_time));
          Serial.println("NOT OKEEEE CAPTURE");
          Serial.println(msg);
        }
      }
      else if((millis()-begin_time)>=7000){
        String responser = httpGETRequest(serverName, id_string);
        Serial.println(responser);
        break;
      }
    }
  },0);
}

void loop()
{
  client.loop();
}
